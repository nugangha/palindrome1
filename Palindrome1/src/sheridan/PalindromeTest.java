package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PalindromeTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	public void testPalindrom() {
		assertTrue("Invalid value for palindrome ", Palindrome.isPalindrome("anna")); 
	}
	@Test
	public void testPalindromNegative() {
		assertFalse("Invalid value for palindrome ", Palindrome.isPalindrome("anna has a lame")); 
	}
	@Test
	public void testPalindromBoundaryIn() {
		assertTrue("Invalid value for palindrome ", Palindrome.isPalindrome("aa")); 
	}
	@Test
	public void testPalindromBoundaryOut() {
		assertFalse("Invalid value for palindrome ", Palindrome.isPalindrome("racer car")); 
	}

}
